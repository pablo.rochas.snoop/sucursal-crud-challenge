package com.electro.sucursalcrud;

import com.electro.sucursalcrud.DAOs.Store;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SucursalCrudApplication.class)
@AutoConfigureMockMvc
public class SucursalCrudApplicationTests
{
	@Autowired
	private MockMvc mvc;

	@Test
	public void testCreateStoreOk() throws Exception
	{
		final Store store = new Store();
		store.setIdStore("idnew1");
		store.setAddress("Address 2345");
		store.setBusinessHours("LUN-VIE 9-20");
		store.setLatitude(34.8765);
		store.setLongitude(65.8764);

		mvc.perform(
				post("/api/stores").contentType(MediaType.APPLICATION_JSON).content(toJson(store)))
				.andExpect(status().isCreated());
	}

	@Test
	public void testCreateStoreError() throws Exception
	{
		final Store store = new Store();
		store.setIdStore("idnew2");
		store.setAddress("Address 2345");
		store.setBusinessHours("LUN-VIE 9-20");
		store.setLatitude(134.8765);	// Valor fuera de rango
		store.setLongitude(65.8764);

		mvc.perform(
				post("/api/stores").contentType(MediaType.APPLICATION_JSON).content(toJson(store)))
				.andExpect(status().is4xxClientError());
	}

	@Test
	public void TestNearestStore() throws Exception
	{
		final Store store = new Store();
		store.setIdStore("id1st");
		store.setAddress("Address 4567");
		store.setBusinessHours("LUN-VIE 9-20");
		store.setLatitude(64.8765);
		store.setLongitude(95.8764);

		mvc.perform(post("/api/stores").contentType(MediaType.APPLICATION_JSON).content(toJson(store)))
				.andExpect(status().isCreated());

		mvc.perform(get("/api/stores/search/findNearestTo?latitude=64&longitude=95"))
				.andExpect(status().isOk())
				.andExpect((jsonPath("$.idStore", is("id1st"))));
	}

	private byte[] toJson(Object obj) throws IOException
	{
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		return mapper.writeValueAsBytes(obj);
	}
}

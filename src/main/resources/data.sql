TRUNCATE TABLE Stores;
INSERT INTO Stores (id, id_Store, address, business_Hours, latitude, longitude) VALUES (20, 'st-1', 'Wazzapamani 853', 'LUN-VIE 9-19', 23.4567, 32.5678);
INSERT INTO Stores (id, id_Store, address, business_Hours, latitude, longitude) VALUES (21, 'st-2', 'Serna 8256', 'LUN-VIE 9-19', 81.5768, -83.0546);
INSERT INTO Stores (id, id_Store, address, business_Hours, latitude, longitude) VALUES (22, 'st-3', 'Arriaga 3774', 'LUN-VIE 9-19', 38.4567, -39.5678);
INSERT INTO Stores (id, id_Store, address, business_Hours, latitude, longitude) VALUES (23, 'st-4', 'Corrales 110', 'LUN-VIE 9-19', 85.4567, 44.5678);
INSERT INTO Stores (id, id_Store, address, business_Hours, latitude, longitude) VALUES (24, 'st-5', 'Coronado 724', 'LUN-VIE 9-19', 76.4567, -78.5678);

TRUNCATE TABLE Drop_Offs;
INSERT INTO Drop_Offs (id, id_Drop_Off_Point, capacity, latitude, longitude) VALUES (20, 'do-1', 5, 87.5678, 77.6789);
INSERT INTO Drop_Offs (id, id_Drop_Off_Point, capacity, latitude, longitude) VALUES (21, 'do-2', 4, 73.5678, 33.6789);
INSERT INTO Drop_Offs (id, id_Drop_Off_Point, capacity, latitude, longitude) VALUES (22, 'do-3', 3, 12.5678, 88.6789);
INSERT INTO Drop_Offs (id, id_Drop_Off_Point, capacity, latitude, longitude) VALUES (23, 'do-4', 7, 24.5678, 89.6789);
INSERT INTO Drop_Offs (id, id_Drop_Off_Point, capacity, latitude, longitude) VALUES (24, 'do-5', 9, 66.5678, 23.6789);

package com.electro.sucursalcrud.repositories;

import com.electro.sucursalcrud.DAOs.Store;
import io.swagger.annotations.ApiParam;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.math.BigDecimal;


@RepositoryRestResource(collectionResourceRel = "stores", path = "stores")
public interface StoreRepository extends CrudRepository<Store, Long>, FindNearestStore
{
    Store findNearestTo(@Param("latitude") @ApiParam(name = "latitude") BigDecimal latitude, @Param("longitude") @ApiParam(name = "longitude") BigDecimal longitude);
}

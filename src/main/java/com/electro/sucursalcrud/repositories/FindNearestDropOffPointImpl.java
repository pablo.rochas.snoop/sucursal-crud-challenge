package com.electro.sucursalcrud.repositories;

import com.electro.sucursalcrud.DAOs.DropOffPoint;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;


import java.math.BigDecimal;
import java.util.Objects;

/**
 * Consideraciones:
 *
 * Esta implementacion calcula la menor distancia euclidea entre puntos de retiro, no es necesariamente la mejor implementacion con coordenadas geográficas.
 * Tambien obtiene la lista de todos los puntos de retiro cada vez, lo cual tampoco escala si hay muchas busquedas y/o sucursales.
 */
public class FindNearestDropOffPointImpl implements FindNearestDropOffPoint
{
    @Autowired
    @Lazy
    private DropOffPointRepository repository;

    @Override
    public DropOffPoint findNearestTo(BigDecimal latitude, BigDecimal longitude)
    {
        final Vector2D point = new Vector2D(latitude.doubleValue(), longitude.doubleValue());
        double nearestDropOffDistance = Double.MAX_VALUE;
        DropOffPoint nearestDropOff = null;

        final Iterable<DropOffPoint> dropOffPoints = repository.findAll();
        for (DropOffPoint d: dropOffPoints)
        {
            Vector2D dropOffPoint = new Vector2D(d.getLatitude(), d.getLongitude());
            final double dropDistance = point.distance(dropOffPoint);
            if (dropDistance < nearestDropOffDistance)
            {
                nearestDropOffDistance = dropDistance;
                nearestDropOff = d;
            }
        }

        if (Objects.isNull(nearestDropOff))
        {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Problem searching nearest drop off point");
        }
        else
        {
            return nearestDropOff;
        }
    }
}

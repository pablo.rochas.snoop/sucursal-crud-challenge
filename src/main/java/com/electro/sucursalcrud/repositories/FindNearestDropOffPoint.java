package com.electro.sucursalcrud.repositories;

import com.electro.sucursalcrud.DAOs.DropOffPoint;
import com.electro.sucursalcrud.DAOs.Store;
import io.swagger.annotations.ApiParam;

import java.math.BigDecimal;

public interface FindNearestDropOffPoint
{
    DropOffPoint findNearestTo(BigDecimal latitude, BigDecimal longitude);
}

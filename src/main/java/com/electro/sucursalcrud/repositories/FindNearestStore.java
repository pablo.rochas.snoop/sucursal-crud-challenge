package com.electro.sucursalcrud.repositories;

import com.electro.sucursalcrud.DAOs.Store;
import io.swagger.annotations.ApiParam;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;

public interface FindNearestStore
{
    Store findNearestTo(BigDecimal latitude, @Param("longitude") @ApiParam(name="longitude") BigDecimal longitude);
}

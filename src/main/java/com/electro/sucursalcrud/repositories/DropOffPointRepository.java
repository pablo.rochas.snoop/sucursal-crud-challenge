package com.electro.sucursalcrud.repositories;

import com.electro.sucursalcrud.DAOs.DropOffPoint;
import io.swagger.annotations.ApiParam;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.math.BigDecimal;

@RepositoryRestResource(collectionResourceRel = "dropoffs", path = "dropoffs")
public interface DropOffPointRepository extends CrudRepository<DropOffPoint, Long>, FindNearestDropOffPoint
{
    DropOffPoint findNearestTo(@Param("latitude") @ApiParam(name = "latitude") BigDecimal latitude, @Param("longitude") @ApiParam(name = "longitude") BigDecimal longitude);
}

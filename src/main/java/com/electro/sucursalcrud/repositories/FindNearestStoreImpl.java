package com.electro.sucursalcrud.repositories;

import com.electro.sucursalcrud.DAOs.Store;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Consideraciones:
 *
 * Esta implementacion calcula la menor distancia euclidea entre las sucursales, no es necesariamente la mejor implementacion con coordenadas geográficas.
 * Tambien obtiene la lista de todas las sucursales cada vez, lo cual tampoco escala si hay muchas busquedas y/o sucursales.
 */
public class FindNearestStoreImpl implements FindNearestStore
{
    @Autowired
    @Lazy
    private StoreRepository storeRepository;

    @Override
    public Store findNearestTo(BigDecimal latitude, BigDecimal longitude)
    {
        final Vector2D point = new Vector2D(latitude.doubleValue(), longitude.doubleValue());
        double nearestStoreDistance = Double.MAX_VALUE;
        Store nearestStore = null;

        final Iterable<Store> stores = storeRepository.findAll();
        for (Store s: stores)
        {
            final Vector2D storePoint = new Vector2D(s.getLatitude(), s.getLongitude());
            final double distancePoint = point.distance(storePoint);
            if (distancePoint < nearestStoreDistance)
            {
                nearestStoreDistance = distancePoint;
                nearestStore = s;
            }
        }

        if (Objects.isNull(nearestStore))
        {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Problem searching nearest store");
        }
        else
        {
            return nearestStore;
        }
    }
}

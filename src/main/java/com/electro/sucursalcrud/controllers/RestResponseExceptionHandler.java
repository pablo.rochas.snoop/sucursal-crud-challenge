package com.electro.sucursalcrud.controllers;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseExceptionHandler extends ResponseEntityExceptionHandler
{
    @ExceptionHandler({org.springframework.web.util.NestedServletException.class,
            org.springframework.transaction.TransactionSystemException.class,
            javax.persistence.RollbackException.class,
            javax.validation.ConstraintViolationException.class})
    protected ResponseEntity<Object> handleValidation(RuntimeException ex, WebRequest req)
    {
        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST, req);
    }
}

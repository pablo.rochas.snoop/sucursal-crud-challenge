package com.electro.sucursalcrud.DAOs;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@Table(name = "DropOffs")
public class DropOffPoint
{
    @Id @GeneratedValue
    private Long id;

    @NotNull
    private String idDropOffPoint;
    @Min(0)
    private int capacity;
    @Min(-90) @Max(90)
    private double latitude;
    @Min(-180) @Max(180)
    private double longitude;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getIdDropOffPoint()
    {
        return idDropOffPoint;
    }

    public void setIdDropOffPoint(String idDropOffPoint)
    {
        this.idDropOffPoint = idDropOffPoint;
    }

    public int getCapacity()
    {
        return capacity;
    }

    public void setCapacity(int capacity)
    {
        this.capacity = capacity;
    }

    public double getLatitude()
    {
        return latitude;
    }

    public void setLatitude(double latitude)
    {
        this.latitude = latitude;
    }

    public double getLongitude()
    {
        return longitude;
    }

    public void setLongitude(double longitude)
    {
        this.longitude = longitude;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        final DropOffPoint that = (DropOffPoint) o;

        return new EqualsBuilder().append(id, that.id).append(latitude, that.latitude).append(longitude, that.longitude).isEquals();
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder(17, 37).append(id).append(latitude).append(longitude).toHashCode();
    }
}

<br />
<p align="center">
  <a href="https://gitlab.com/pablo.rochas.snoop/sucursal-crud-challenge">
    <h2 align="center">Sucursal CRUD</h2> 
  </a>
  
## Contenido

- [Contenido](#contenido)
- [Ejercicio](#ejercicio)
- [Dependencias](#dependencias)
- [Comienzo rápido](#comienzo-rápido)
  - [Prerequisitos](#prerequisitos)
  - [Instalación y ejecución](#instalación-y-ejecución)
- [Uso](#uso)
- [Contacto](#contacto)

## Ejercicio

   Para mejorar la experiencia del cliente, la sucursal default que se ofrece como punto de retiro debe ser la más cercana a su ubicación. Para esto, una de las
   funcionalidades que se necesita es conocer la sucursal más cercana a un punto dado (latitud y longitud). 
   Diseñar e implementar un servicio que exponga en su API las operaciones CRUD (creación, edicion, borrado y lectura por id) de las entidades `SUCURSAL` y `Punto de retiro` junto con la consulta del nodo más cercana a un punto dado.


## Dependencias

* [Spring Boot](https://spring.io/projects/spring-boot)
* [Spring Data](https://spring.io/projects/spring-data)
* [SpringFox](https://springfox.github.io/springfox/)
* [Postgresql](https://www.postgresql.org/)


## Comienzo rápido

### Prerequisitos

Este es el software que ya deberías tener instalado para probar la aplicación:
* GIT
* Java 8+
* [Maven](http://maven.org)
* [Docker](https://docs.docker.com/get-docker/)
* [Docker Compose](https://docs.docker.com/compose/install/)

### Instalación y ejecución

1. Clonar el repositorio
```sh
git clone https://gitlab.com/pablo.rochas.snoop/sucursal-crud-challenge
```

2. Armar la imagen de Docker del servicio. Esta imagen no se arma a partir de un `Dockerfile`, sino que lo hace a partir del plugin de Maven provisto por Spring Boot. Esa imagen está basada en un buildpack de [Paketo](https://packaeto.io)
```sh
mvn clean package
```
Esto crea una imagen de Docker de la aplicación en el repositorio local con el nombre `sucursalcrud`

3. Indicar el inicio de los componentes usando `docker-compose`: 
```sh
docker-compose up
```
Esto descarga las dependencias y ejecuta los servicios.

## Uso

Una vez ejecutado, el sistema atiende operaciones HTTP en el puerto 8080. La API relacionada a las sucursales se encuentra en la ruta [`/api/stores`](http://localhost:8080/api/stores) y la relacionada a puntos de retiro, en [`/api/dropoffs`](http://localhost:8080/api/dropoffs). La documentación de ambas se puede encontrar en formato Swagger en la URL [`/v2/api-docs`](http://localhost:8080/v2/api-docs). También hay una interfaz web en [`/swagger-ui`](http://localhost:8080/swagger-ui/). En el puerto 8081 hay una instancia de [Adminer](https://www.adminer.org) para verificar los cambios la BD. En la raiz del proyecto hay una colección de Postman con ejemplos de prueba. Hay también un health-check del servicio en [/actuator/health](http://localhost:8080/actuator/health).


## Contacto

Pablo Rochás - pablo.rochas@snoopconsulting.com
